// textConstants.h
//
// The code in this file is part of Pyxplot
// <http://www.pyxplot.org.uk>
//
// Copyright (C) 2006-2012 Dominic Ford <coders@pyxplot.org.uk>
//               2008-2012 Ross Church
//
// $Id: textConstants.h 1261 2012-07-11 21:38:05Z dcf21 $
//
// Pyxplot is free software; you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation; either version 2 of the License, or (at your option) any later
// version.
//
// You should have received a copy of the GNU General Public License along with
// Pyxplot; if not, write to the Free Software Foundation, Inc., 51 Franklin
// Street, Fifth Floor, Boston, MA  02110-1301, USA

// ----------------------------------------------------------------------------

#ifndef _TEXTCONSTANTS_H
#define _TEXTCONSTANTS_H 1

extern char ppltxt_version[];
extern char ppltxt_help[];
extern char ppltxt_welcome[];
extern char ppltxt_invalid[];
extern char ppltxt_valid_set_options[];
extern char ppltxt_set_noword[];
extern char ppltxt_unset_noword[];
extern char ppltxt_set[];
extern char ppltxt_unset[];
extern char ppltxt_show[];
void   ppltxt_init   ();

#endif

